#!/usr/bin/python3

import jsonlines, os, twitter, logging, argparse, datetime

from data.api_keys import *

api = twitter.Api(consumer_key=consumer_key,
                  consumer_secret=consumer_secret,
                  access_token_key=access_token,
                  access_token_secret=access_token_secret)

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--pretend", action="store_true")
parser.add_argument("target", nargs="?", default="")
args = vars(parser.parse_args())

logging.basicConfig(format="%(asctime)s %(message)s",
                    level=logging.INFO,
                    datefmt="[%Y-%m-%d %H:%M]")

base_dir = "/opt/foch-toute/"
data_dir = base_dir + "data/"

f_dico = data_dir + "DEM.jsonl"
f_prev = data_dir + "prev"

articles = {"M": "le ", "F": "la "}

if not os.path.exists(f_prev):
    
    msg  = "c'est parti"
    w_ID = 0
    
else:
    
    with open(f_prev) as f:
    
        msg  = ""
        ID   = int(f.read())

    with jsonlines.open(f_dico) as dictionary:

        nb   = 0
        prev = ""
        
        for word in dictionary:
            
            wd   = word["M"]
            w_ID = word["ID"]
            
            if wd.find(" ") == -1 and wd != prev:
                
                prev = wd
                
                if w_ID > ID:
                    
                    nb += 1
                
                    if not args["target"]:
                
                        cat = word["CA"]["categorie"]
                        
                        if cat == "N":
                            
                            if wd[-1] in "sx":
                                prefix = "les "
                            elif wd[0] in "aàâeéèêiouy":
                                prefix = "l'"
                            else:
                                prefix = articles[word["CA"]["genre"][0]]

                        elif cat == "A":
                            
                            prefix = "ce qui est "

                        elif cat[0] == "V":
                            
                            prefix = "le fait d"
                            
                            if cat[1] == "p":
                                prefix += "e s"

                            if wd[0] in "aàâeéèêiouy":
                                prefix += "'"
                            else:
                                prefix += "e "

                        else:
                            
                            prefix = ""
                        
                        msg = "Foch %s%s" % (prefix, wd)
                        
                        break
                        
                    elif wd == args["target"]:
                        
                        dt = datetime.datetime.now()
                        moment = dt + datetime.timedelta(minutes = 15 * nb - dt.minute % 15 )
                        msg += "{:%Y-%m-%d %H:%M}".format(moment)
                        break

if not msg:
    
    if args["target"]:
    
        msg = "never"
        
    else:
        
        msg = "finito"

if args["target"]:
    
    log = "Target"
    msg = "%s %s" % (args["target"], msg) 
    
elif args["pretend"]:
    
    log = "Pretend"
    
else:

    try:
        
        api.PostUpdate(msg)
        
        log = "Tweet"
        
        with open(f_prev, "w") as f:
            f.write(str(w_ID))
        
    except:
        
        log = "Probreem"

logging.info("%s: %s" % (log, msg))
