# Foch toute

Twitter bot qui [@FochToute](https://twitter.com/FochToute/)

Fortement inspiré par [@fckeveryword](https://twitter.com/fckeveryword)

## data (gitignore)

- [Dictionnaire Électronique des Mots](http://rali.iro.umontreal.ca/rali/fr/DEM-json) du RALI:

`wget http://rali.iro.umontreal.ca/LVF_DEM/DEM.jsonl` (34 Mo)

- `api_keys.py`

- `prev`: ID du dernier mot tweeté

- `foch.log`

## python

`pip3 install jsonlines python-twitter logging argparse datetime`

## crontab

Un mot par 15 minutes (~ 3,5 ans pour passer à travers le dictionnaire)

`*/15 * * * * /opt/foch-toute/foch.py >> /opt/foch-toute/data/foch.log 2>&1`

## usage

- `./foch.py` tweete le prochain mot

- `./foch.py -p` ne fait que l'afficher (sans mise à jour de `prev`)

- `./foch.py target` prédit à quel moment `target` sera tweeté

## événements notables

passés et futurs

- `a` le 2020-10-07 8:00 (GMT+1)

- 2020-10-16 [panne Twitter](https://www.lemonde.fr/pixels/article/2020/10/16/twitter-victime-d-une-panne-mondiale_6056196_4408996.html) pendant 3h30

- 2020-10-18 panne électrique maison (durée 10h)

- 2020-10-28 redémarrage, 1 mot raté

- 2020-11-10 00:53 Probreem: Foch l'alphanesse ?

- 2020-12-23 box débranchée

- 2021-04-17 rattrapage de petits glitchs

- `loup` le 2022-08-03 07:30 (GMT+1) à vérifier

- `maréchal` le 2022-08-23 22:15 (GMT+1) à vérifier

- `police` le 2023-03-19 09:15 (GMT+1)

- `zyzomys` le 2024-02-12 21:15 (GMT+1)

Et après ? Foch ze English language ??
